#include <EasyZip.h>
#include <zip.h>
#include <iostream>

namespace EasyZip
{

Zip::Zip(void)
{
    HandleArchive = NULL;
}

Zip::Zip(String const &FilePath, ILogger * logProccessor) : LogProccessor(logProccessor)
{
    HandleArchive = CreateZip(FilePath.c_str(), NULL);
    if(HandleArchive == NULL)
    {
        throw(std::exception("Can't create archive"));
    }
    this->ArchiveFilePath = FilePath;
}
Zip::Zip(String const &FilePath, ZIP_LVL ZipLevel, ILogger * logProccessor) : LogProccessor(logProccessor)
{
    InitZipLevel(ZipLevel);
    HandleArchive = CreateZip(FilePath.c_str(), NULL);
    if(HandleArchive == NULL)
    {
        throw(std::exception("Can't create archive"));
    }
    this->ArchiveFilePath = FilePath;
}

Zip::~Zip(void)
{
    if(IsErrorOpen() == false)
        Close();
}

bool Zip::AddFileInArchive(String FilePath, String NewFileName)
{
    if(NewFileName.empty())
    {
        NewFileName = GetFileName(FilePath);
    }
    if(IsErrorOpen())
        LastError = ERROR_HANDLES_CLOSED;
    else
        LastError = ZipAdd(reinterpret_cast<HZIP>(HandleArchive), NewFileName.c_str(), FilePath.c_str());

    if(LastError != ZR_OK)
    {
        String errStr = STR("EasyZip: Can't add file: ");
        errStr.append(FilePath);
        Log(errStr, true);
        return false;
    }

    return true;
}
bool Zip::AddFile(String const &FilePath)
{
    return AddFileInArchive(FilePath);
}

bool Zip::AddFile(String const &FilePath, String const &NewFileName)
{
    return AddFileInArchive(FilePath, NewFileName);
}

bool Zip::IsErrorOpen()
{
    return HandleArchive == NULL ? true : false;
}

bool Zip::Close()
{
    ArchiveFilePath.clear();
    LastError = CloseZip(reinterpret_cast<HZIP>(HandleArchive));
    if(LastError != ZR_OK)
    {
        String errStr = STR("EasyZip: ");
        Log(errStr, true);
        return false;
    }
    HandleArchive = NULL;
    LogProccessor = NULL;
    LastError = ERROR_SUCCESS;
    return true;
}
UnZip::UnZip(void)
{
}


UnZip::~UnZip(void)
{
}
unsigned int Zip::Log(bool error)
{
    return Log(STR(""), error);
}
unsigned int Zip::Log(String const &msg, bool error)
{
    unsigned int errorCode = LastError;
    String ErrorMessage = STR("EasyZip:");
    ErrorMessage.append(msg);

    if(LogProccessor == NULL)
    {
        #ifdef UNICODE
            if(error)
                std::wcerr << ErrorMessage << L" #" << errorCode << std::endl;
            else
                std::wcerr << ErrorMessage << std::endl;
        #else
            if(error)
                std::cerr << ErrorMessage << " #" << errorCode << std::endl;
            else
                std::cerr << ErrorMessage << std::endl; 
        #endif
        

        throw std::exception("EasyZip");
        return 0;
    }
    if(error)
        return LogProccessor->append(ErrorMessage, errorCode);
    else
        return LogProccessor->append(ErrorMessage);
}

String GetFileName(String FilePath)
{
    if(FilePath.empty())
        return FilePath;
    String::size_type pos = FilePath.find_last_of(_T("\\"));
    if(pos == String::npos)
        return FilePath;
    pos++;
    return FilePath.substr(pos, FilePath.length() - pos);
}

}