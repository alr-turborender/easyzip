#pragma once
#include <string>
#include <Logger.h>

namespace EasyZip
{
#ifdef UNICODE
    typedef std::wstring String;
    #define STR(x)      L ## x
#else
    typedef std::string String;
    #define STR(x)      x
#endif

    String GetFileName(String FilePath);
    enum ZIP_LVL {LV0, LV1, LV2, LV3, LV4, LV5, LV6, LV7, LV8, LV9};

    class Zip
    {
    private:
        String ArchiveFilePath;
        void * HandleArchive;
        unsigned int LastError;
        ILogger * LogProccessor;
    protected:
        Zip(void);
        //Zip(Zip const &);
        //Zip & operator=(Zip const &);

        unsigned int Log(bool error = false);
        unsigned int Log(String const &msg, bool error = false);
        
        bool AddFileInArchive(String FilePath, String NewFileName = STR(""));
        
    public:
        
        Zip(String const &ArchiveFilePath, ILogger * logProccessor = NULL);
        Zip(String const &ArchiveFilePath, ZIP_LVL ZipLevel, ILogger * logProccessor = NULL);
        ~Zip(void);

        bool AddFile(String const &FilePath);
        bool AddFile(String const &FilePath, String const &NewFileName);

        bool IsErrorOpen();

        bool Close();

        unsigned int GetLastError() { return LastError; }
    };

    class UnZip
    {
    public:
        UnZip(void);
        ~UnZip(void);
    };
}

